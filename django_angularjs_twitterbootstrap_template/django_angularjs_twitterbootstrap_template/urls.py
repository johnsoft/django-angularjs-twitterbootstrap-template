from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'django_angularjs_twitterbootstrap_template.views.home', name='home'),
    # url(r'^django_angularjs_twitterbootstrap_template/', include('django_angularjs_twitterbootstrap_template.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # App Main mit eigenen URL's
    url(r'^$', include('main.urls', namespace="main")),
)
