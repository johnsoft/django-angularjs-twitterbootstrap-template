from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.core.urlresolvers import reverse
from django.http import Http404
from django.template import RequestContext, loader

def index(request):
    return render(request, 'main/index.html')
